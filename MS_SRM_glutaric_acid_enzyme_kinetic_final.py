import os
import re
import peakutils
import numpy as np
import matplotlib.pyplot as plt


class MSAnalyzer:

    def __init__(self) -> None:
        self.store = {}

    def __call__(self) -> None:
        """Start processing txt files"""
        self.create_folders(['data', 'csv'])
        file_names = os.listdir('./data')
        for file_name in file_names:
            if file_name.endswith('.txt'):
                key = self.correcting_file_name(file_name)
                with open(f'./data/{file_name}', 'r') as f:
                    raw_data = f.readlines()
                    self.get_time(raw_data, key)
                    self.get_ions(raw_data, key)
                    self.get_startpoint(raw_data, key)
                    self.calc_area_under_curve(key)
                    self.plot_integrated_peak(key)    # can be activated to check if signal processing works properly
                    self.export_to_csv(key)


    @staticmethod
    def create_folders(folders: list) -> None:
        """Check and create folders"""
        for folder in folders:
            os.makedirs(folder, exist_ok=True)
        if len(os.listdir('./data')) == 0:
            raise FileNotFoundError('Expected ".txt" files in data folder')

    @staticmethod
    def convert_to_seconds(time_str: str) -> int:
        """Convert time to seconds"""
        h, m, s = time_str.split(':')
        return int(h) * 3600 + int(m) * 60 + int(s)

    @staticmethod
    def correcting_file_name(file_name: str) -> str:
        """Clean up file name numbering"""
        file_name, _ = os.path.splitext(file_name)
        findings = re.findall(r'\(\d+\)', file_name)  # get number between ()
        if not findings:
            return f'{file_name.strip()}0'
        if len(findings) == 1:
            tmp = findings[0]
            number = tmp[1:-1]  # strip away ()
            file_name = file_name.replace(findings[0], '').strip()
            return f'{file_name}{number}'
        raise ValueError(f'Not able to handle file number findings: {findings}')

    def get_time(self, raw_data: list, key: str) -> None:
        """Read time from data"""
        for line in raw_data:
            if 'id: TIC' in line:
                y = raw_data[raw_data.index(line) + 5]
        y_clean = y[24:len(y) - 2].split(' ')
        tmp = []
        for i in y_clean:
            tmp.append(float(i))
        self.store[f'{key}_time'] = np.array(tmp)

    def get_startpoint(self, raw_data: list, key: str) -> None:
        """Get the start time point for enzyme kinetics"""
        for line in raw_data:
            if 'startTimeStamp:' in line:
                measuring_date = raw_data[raw_data.index(line)]
        measuring_time = measuring_date[31:len(measuring_date) - 2]
        self.store[f'{key}_timepoint'] = self.convert_to_seconds(measuring_time)

    def get_ions(self, raw_data: list, key: str) -> None:
        """Read ion count"""
        srms = []
        quadro = []
        for line in raw_data:
            self.counter = 0
            if 'id: TIC' in line:
                y = raw_data[raw_data.index(line) + 8]
                yclean = y[24:len(y) - 2].split(' ')
                tmp = []
                for i in yclean:
                    tmp.append(float(i))
                self.store[f'{key}_ion_total'] = np.array(tmp)

            if 'id: - SRM SIC' in line:
                srms.append(raw_data[raw_data.index(line) + 20])
                quadro.append(line)

        self.store[f'{key}_srm_names'] = []
        for measure in quadro:
            z = measure[0:len(measure)-1]
            self.store[f'{key}_srm_names'].append(z)

        self.store[f'{key}_srms'] = []
        for srm in srms:
            y = srm[24:len(srm) - 2].split(' ')
            tmp = []
            for i in y:
                tmp.append(float(i))
            self.store[f'{key}_srms'].append(np.array(tmp))

    def calc_area_under_curve(self, key: str) -> None:
        """Calculate area under curve"""
        #time = self.store[f'{key}_time']
        self.store[f'{key}_int_start'] = 0.0  # defines at what point the peak integration starts, depends on the dead volume of the setup
        self.store[f'{key}_int_end'] = 0.2  #  defines at what point the peak integration ends (typically after right after the peak)
        self.store[f'{key}_srm'] = []
        self.store[f'{key}_counter'] = 0
        self.store[f'{key}_signal_corrected'] = []
        self.store[f'{key}_base'] = []
        self.store[f'{key}_normalized'] = []
        self.store[f'{key}_int_time'] = []

        for srm in self.store[f'{key}_srms']:
            self.store[f'{key}_base'].append(peakutils.baseline(srm, max_it=20, deg=3, tol=0.1))  # degree of polynomial fit
            self.store[f'{key}_signal_corrected'].append(srm - self.store[f'{key}_base'][self.store[f'{key}_counter']])  # substract fitted baseline from signal
            y_cut = self.store[f'{key}_signal_corrected'][self.store[f'{key}_counter']][np.logical_and(self.store[f'{key}_time'] >= self.store[f'{key}_int_start'],self.store[f'{key}_time'] <= self.store[f'{key}_int_end'])]  # chop the baseline adjusted graph by using int_start, int_end boundaries
            self.store[f'{key}_srm'].append(y_cut)
            self.store[f'{key}_int_time'].append(self.store[f'{key}_time'][np.logical_and(self.store[f'{key}_time'] >= self.store[f'{key}_int_start'],self.store[f'{key}_time'] <= self.store[f'{key}_int_end'])]) # adjust the time array to be within the integration barriers int_start and int_end
            srm_area = int(np.sum(y_cut))  # Integrate the baseline adjusted area between int_start and int_end
            self.store[f'{key}_normalized'].append(srm_area)
            self.store[f'{key}_counter'] += 1

    def plot_integrated_peak(self, key: str) -> None:
        for i in range(self.store[f'{key}_counter']):
            plt.title({self.store[f'{key}_srm_names'][i]})
            plt.plot(self.store[f'{key}_time'], self.store[f'{key}_srms'][i])
            plt.plot(self.store[f'{key}_int_time'][i],self.store[f'{key}_srm'][i])
            plt.plot(self.store[f'{key}_time'], self.store[f'{key}_base'][i])
            plt.show()


    def export_to_csv(self, key: str) -> None:
        with open(os.path.join('csv', 'results.csv'), 'a') as csv_out:
            csv_out.write('sample_name;time(s);int_start;int_end;srm_name;area\n')
            for i in range(self.store[f'{key}_counter']):
                csv_out.write(f"{key};{self.store[f'{key}_timepoint']};{self.store[f'{key}_int_start']};{self.store[f'{key}_int_end']};{(self.store[f'{key}_srm_names'][i])};{self.store[f'{key}_normalized'][i]}\n")


if __name__ == '__main__':
    analyzer = MSAnalyzer()
    analyzer()
