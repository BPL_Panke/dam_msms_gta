ENZYME ACTIVITY ASSAY BASED ON QUANTIFYING GLUTARIC ACID (GTA)
----------------------------------------------------------------------------------------------------------------------------------------

EXPERIMENTAL PROCEDURE:

Hydrolysis of GTA-conjugates was determined by measuring the concentration of released glutaric acid using MS/MS. Free glutaric acid was quantified using a selected reaction monitoring (SRM) method, tracking the transition of the negatively charged molecule ion (m/z = 131) to the decarboxylated fragment (m/z = 87) in negative mode using a QTRAP4000 MS/MS (AB Sciex LLC, Famingham, USA). To quantify the reaction product, a fixed concentration of isotopically labelled d4-glutaric acid (2,2,4,4-D4, Cambridge Isotope Laboratories, Tewksbury, USA) was added to the reaction mixture and used for normalization. The integrated signal obtained from the product transition m/z = 131 -> m/z = 87 was normalized with the corresponding transition of the stable isotope standard m/z = 135 -> m/z = 91 and compared to a calibration curve. In order to track the reaction progress, a 1 mL sample containing typically 1 mM GTA-conjugate, 200 µM d4-glutaric acid (2,2,4,4-D4) and 100 nM purified enzyme exhibiting glutaryl acylase activity in 10 mM aqueous NH4HCO3 adjusted to pH 7.4 with formic acid was incubated at 37°C in an autosampler unit of an HPLC. The outlet tubing of the autosampler unit was directly connected to the interface of the QTRAP4000 MS/MS device, without using an HPLC column. The same sample was injected several times over the time course of 4 hours (10µL/injection) directly into mobile phase (700 µL min-1, 50:50 v/v acetonitrile/water) and was subsequently analyzed via MS/MS. The following QTRAP4000 settings were used: IonSpray Voltage(IS): -4,500, Temperature (TEM): 750, Ion Source Gas 1 & 2 (GS1, GS2): 30, Collision Gas (CAD): 4, Declustering Potential (DP): -65 and Collision Energy (CE): -18.

----------------------------------------------------------------------------------------------------------------------------------------

DATA PROCESSING:

The obtained raw data file “.wiff” was converted with msConvert (ProteoWizard, Version: 3.0.19217-f7f3a630b) into rich “.txt” files and MS/MS data is extracted using this python script. The python script yields a .csv spreadsheet, containing sample name, measuring time point in seconds relative to start time and integrated peak area of analyte and internal standard. 

HOW TO USE THE SCRIPT:
1) Put exported ".txt" files (derived from msConvert) of the kinetic measurement in a folder named "data" and execute the MS_SRM_glutaric_acid_enzyme_kinetic_final.py script within the same directory.
2) The script will generate a folder named "CSV" containing the processed data file

----------------------------------------------------------------------------------------------------------------------------------------

Example data can be found in folder "data"
